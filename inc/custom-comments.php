<?php

function wp_bootstrap_4_comment( $comment, $args, $depth ) {
    ?>

    <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">

        <?php
            $wb_gravatar_url = get_avatar_url( $comment );
        ?>
        <article class="media wp-bootstrap-4-comment">
            <?php if (! empty( $wb_gravatar_url ) ) : ?>
                <img class="d-flex align-self-start mr-4 comment-img rounded" src="<?php echo esc_url( $wb_gravatar_url ); ?>" alt="<?php echo esc_attr( get_comment_author() ); ?>" width="60">
            <?php endif; ?>
            <div class="media-body">
                <h6 class="mt-0 mb-0 comment-author">
                    <?php echo get_comment_author_link(); ?>
                    <?php if ( $comment->comment_author_email == get_the_author_meta( 'email' ) ) : ?>
                        <small class="wb-comment-by-author ml-2 text-muted"><?php echo esc_html__( '&#8226; Post Author &#8226;', 'wp-bootstrap-4' ) ?></small>
                    <?php endif; ?>
                </h6>
                <small class="date text-muted"><?php printf( // WPCS: XSS OK.
                                                        /* translators: %1 %2: date and time. */
                                                        esc_html__('%1$s at %2$s', 'wp-bootstrap-4'),
                                                        get_comment_date(),
                                                        get_comment_time()
                                                    ); ?></small>
                <?php if ($comment->comment_approved == '0') : ?>
					<small><em class="comment-awaiting text-muted"><?php esc_html_e('Comment is awaiting approval', 'wp-bootstrap-4'); ?></em></small>
					<br />
				<?php endif; ?>

                <div class="mt-3">
                    <?php comment_text(); ?>
                </div>

                <?php
                    $args['before'] = '';
                ?>

                <small class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'wp-bootstrap-4' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ), $comment->comment_ID ); ?>
					<?php edit_comment_link( esc_html__( 'Edit', 'wp-bootstrap-4' ) ); ?>
				</small>
            </div>
            <!-- /.media-body -->
        </article>

    <?php
}


function wp_custom_comment( $comment, $args, $depth ) {
    ?>

    <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">

        <article class="media wp-bootstrap-4-comment single-comment">
            <div class="media-body single-comment__body">
                <h6 class="mt-0 mb-0 comment-author single-comment__author">
                    <?php echo get_comment_author_link(); ?>
                </h6>
                <?php
                    $publish_date = strtotime(get_comment_date('d-m-Y'));
                    $current_date = strtotime(date('d-m-Y'));

                    //Calc days
                    if($publish_date && $current_date && ($current_date - $publish_date) / 86400 <= 7):
                        $date_output = ($current_date - $publish_date) / 86400;
                        
                        //Append appropriate sentance
                        if($date_output !== 1):
                            $date_output .= ' dagen geleden';
                        else:
                            $date_output .= ' dag geleden';
                        endif;
                    else:
                        $date_output = get_comment_date('d M Y');
                    endif;
                ?>
                <small class="single-comment__date"><?php printf( // WPCS: XSS OK.
                                                        /* translators: %1 %2: date and time. */
                                                        esc_html__('%1$s', 'wp-bootstrap-4'),
                                                        $date_output
                                                        
                                                    ); ?></small>
                <small class="single-comment__likes">
                    <i class="far fa-thumbs-up"></i>
                    <span class="single-comment__likes--number">
                    <?php
                        $comment_likes = get_field('comment_likes', get_comment($comment->ID));

                        echo $comment_likes ? $comment_likes : 0;
                    ?>
                    </span>
                </small>
                <?php if ($comment->comment_approved == '0') : ?>
					<small><em class="comment-awaiting text-muted"><?php esc_html_e('Comment is awaiting approval', 'wp-bootstrap-4'); ?></em></small>
					<br />
				<?php endif; ?>

                <div class="single-comment__text mt-3">
                    <?php comment_text(); ?>
                </div>

                <?php
                    $args['before'] = '';
                ?>
            </div>
            <!-- /.media-body -->
            <div class="single-comment__actions">
                <a href="#" class="single-comment__like">Like</a>
                &#8226;
                <?php
                    //if($comment->comment_parent == 0):
                        comment_reply_link(
                            array_merge(
                                $args,
                                array(
                                    'reply_text' => esc_html__( 'Reageren', 'wp-bootstrap-4' ),
                                    'depth' => $depth,
                                    'max_depth' => $args['max_depth']
                                )
                            ),
                            $comment->comment_ID
                        );
                    //endif;
                ?>
                <?php edit_comment_link( esc_html__( 'Bewerken', 'wp-bootstrap-4' ) ); ?>

                <?php
                    $child_args = array(
                        'parent' => $comment->comment_ID,
                        'hierarchical' => true,
                       );
                    $count = count(get_comments($child_args));

                    if($comment->comment_parent == 0 && $count > 0):
                        
                        ?>
                        <a href="#" id="toggle_comments_<?= $comment->comment_ID; ?>" class="single-comment__reacties"><?= $count ?> Reacties</a>
                        <?php
                    endif;
                ?>
            </div>
        </article>                                       
    <?php
}
