<?php 


/** ADD CUSTOM COLUMNS TO THE COMPETITION OVERVIEW IN THE ADMIN DASHBOARD */
// add_filter( 'manage_competities_posts_columns', 'vu_filter_posts_columns' );
function vu_filter_posts_columns( $columns ) {
    $columns = array(
        'cb' => $columns['cb'],
        'title' => __('API Titel'),
        'seizoen' => __( 'Seizoen' ),
        'titel' => __( 'Titel' ),
        'actief' => __('Actief'),
        'date' => __('Date')
    );
    return $columns;
}
// add_action( 'manage_competities_posts_custom_column', 'vu_competities_column', 10, 2);
function vu_competities_column( $column, $post_id ) {
    if ( 'titel' === $column ) {
        echo get_field('competitie_titel', $post_id);
    }
    if ( 'seizoen' === $column ) {
        echo get_field('season', $post_id);
    }
    if( 'actief' === $column) {
        echo get_field('is_actief', $post_id)[0] === 'yes' ? 'ja' : 'nee';
    }
}

// add_filter( 'manage_edit-competities_sortable_columns', 'vu_competities_sortable_columns');
function vu_competities_sortable_columns( $columns ) {
    $columns['titel'] = 'titel';
    $columns['seizoen'] = 'seizoen';
    $columns['actief'] = 'actief';
    return $columns;
}

/** Make the columns sortable */
// add_action( 'pre_get_posts', 'vu_posts_orderby' );
function vu_posts_orderby( $query ) {
    if( ! is_admin() || ! $query->is_main_query() ) {
        return;
    }

    if ( 'seizoen' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set('meta_key', 'competition_season');
        $query->set( 'meta_type', 'numeric' );
    }

    if ( 'titel' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'competition_name'); //field_5d5d3b745c9d7
        $query->set( 'meta_type', 'numeric' );
        $query->set( 'meta_type', 'char' );
    }
    if ( 'actief' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'is_actief');
        $query->set( 'meta_type', 'numeric' );
        $query->set( 'meta_type', 'char' );
    }
}
/** END OF CUSTOM COLUMNS IN THE COMPETITION OVERVIEW */


/** ADD CUSTOM COLUMNS TO THE COMPETITION OVERVIEW IN THE ADMIN DASHBOARD */
// add_filter( 'manage_teams_posts_columns', 'vu_filter_teams_columns' );
function vu_filter_teams_columns( $columns ) {
    $columns = array(
        'cb' => $columns['cb'],
        'title' => __('API Titel'),
        'soort' => __('Soort'),
        'type' => __('Type'),
        'land' => __('Land'),
        'club_actief' => __('Actief'),
        'date' => __('Date')
    );
    return $columns;
}
// add_action( 'manage_teams_posts_custom_column', 'vu_teams_column', 10, 2);
function vu_teams_column( $column, $post_id ) {
    if ( 'soort' === $column ) {
        $kind = get_field('soort_team', $post_id)[0];
        if($kind === 'c') {
            echo 'club';
        } else if($kind === 'n') {
            echo 'national';
        } else {
            echo $kind;
        }
    }
    if ( 'type' === $column ) {
        $type = get_field('team_type', $post_id)[0];
        if($type === 'women') $type = 'Vrouwen';
        if($type === 'youth') $type = 'Jeugd';
        if($type === 'default') $type = 'Mannen';
        if($type === 'd') $type = 'Mannen';
        echo $type;
    }
    if ( 'land' === $column ) {
        echo get_field('land', $post_id);
    }
    if( 'club_actief' === $column ) {
        echo get_field('club_actief', $post_id) === true ? 'Ja' : 'Nee';
    }
}

// add_filter( 'manage_edit-teams_sortable_columns', 'vu_teams_sortable_columns');
function vu_teams_sortable_columns( $columns ) {
    $columns['soort'] = 'soort';
    $columns['type'] = 'type';
    $columns['land'] = 'land';
    $columns['club_actief'] = 'club_actief';
    return $columns;
}

/** Make the columns sortable */
// add_action( 'pre_get_posts', 'vu_teams_orderby' );
function vu_teams_orderby( $query ) {
    if( ! is_admin() || ! $query->is_main_query() ) {
        return;
    }

    if ( 'soort' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'soort_team');
        $query->set( 'meta_type', 'numeric' );
        $query->set( 'meta_type', 'char' );
    }

    if ( 'type' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'team_type');
        $query->set( 'meta_type', 'numeric' );
        $query->set( 'meta_type', 'char' );
    }

    if ( 'land' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'land');
        $query->set( 'meta_type', 'numeric' );
        $query->set( 'meta_type', 'char' );
    }
    if ( 'club_actief' === $query->get( 'orderby') ) {
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'meta_key', 'club_actief');
        $query->set( 'meta_type', 'char' );
    }
}
/** END OF CUSTOM COLUMNS IN THE COMPETITION OVERVIEW */

?>