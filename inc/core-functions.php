<?php

// Register navigation
register_nav_menus( array(
    'header' => 'Hoofdmenu',
    'footer-1' => 'Footer 1',
    'footer-2' => 'Footer 2',
    'footer-horizontal' => 'Footer horizontaal'
    )
);

// Register Sidebar widgets
function WPStarter_register_sidebars() {

    // Register the primary sidebar
    register_sidebar(
        array(
            'id' => 'sidebar-1',
            'name' => __( 'Default Sidebar', 'WPStarter' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5 class="widget-title">',
            'after_title' => '</h5>'
        )
    );
    
    // Blog sidebar
    register_sidebar( array(
        'id' => 'sidebar-2',
        'name' => __( 'Blog Sidebar', 'WPStarter' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="widget-title">',
        'after_title' => '</h5>'
    ) );

    /* Repeat register_sidebar() code for additional sidebars. */
}
// add_action( 'widgets_init', 'WPStarter_register_sidebars' );

// Register footer widgets
function footer_widgets_init() {
    // register_sidebar( array(
    //     'name'          => 'Footer 1',
    //     'id'            => 'footer-1',
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget'  => '</div>',
    //     'before_title'  => '<h5 class="ttl">',
    //     'after_title'   => '</h5>',
    // ) );
    // register_sidebar( array(
    //     'name'          => 'Footer 2',
    //     'id'            => 'footer-2',
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget'  => '</div>',
    //     'before_title'  => '<h5 class="ttl">',
    //     'after_title'   => '</h5>',
    // ) );
    register_sidebar( array(
        'name'          => 'Footer 3',
        'id'            => 'footer-3',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="footer__heading">',
        'after_title'   => '</div>',
    ) );
    // register_sidebar( array(
    //     'name'          => 'Footer horizontaal',
    //     'id'            => 'footer-horizontal',
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget'  => '</div>',
    //     'before_title'  => '<h5 class="ttl">',
    //     'after_title'   => '</h5>',
    // ) );

}
add_action( 'widgets_init', 'footer_widgets_init' );

// Add post thumbnail
add_theme_support( 'post-thumbnails' );

// Max excerpt lenght
function wpstarter_change_excerpt_length($length) {
    return 75;
}
add_filter('excerpt_length', 'wpstarter_change_excerpt_length');

// Note that this intentionally disables a tinyMCE security feature.
// Use of this code is NOT recommended.
add_filter('tiny_mce_before_init','tinymce_allow_unsafe_link_target');
function tinymce_allow_unsafe_link_target( $mceInit ) {
    $mceInit['allow_unsafe_link_target']=true;
    return $mceInit;
}

// add classes to images
function nddt_add_class_to_images($class){
    $class .= ' img-fluid lazyload';
    return $class;
}
add_filter('get_image_tag_class','nddt_add_class_to_images');

// add featured images to RSS Feeds
function rss_post_thumbnail($content) {
    global $post;
    if(has_post_thumbnail($post->ID)) {
    $content = '<p>' . get_the_post_thumbnail($post->ID) .
    '</p>' . get_the_content();
    }
    return $content;
}

add_filter('the_excerpt_rss', 'rss_post_thumbnail');
add_filter('the_content_feed', 'rss_post_thumbnail');



//https://developer.wordpress.org/reference/functions/wp_get_nav_menu_items/
function wp_menu_route($data) {
    $menuLocations = get_nav_menu_locations(); 
    $menuID = $menuLocations['footer-' . $data['id']]; // Get the *primary* menu added in register_nav_menus()
    $navObj = wp_get_nav_menu_object($menuID); // Get the array of wp objects, the nav items for our queried location.
    $navItems = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.

    foreach($navItems as $item) {
        $item->slug = basename(get_permalink($item->ID));
    }
    return array(
        'menudata' => $navObj,
        'items' => $navItems
    );
}

function wp_menu_all() {
    $menuLocations = get_nav_menu_locations();
    return $menuLocations;
}

add_action( 'rest_api_init', function () {
    //https://your-wp-domain-url.com/wp-json/custom-name/menu
    register_rest_route( 'footer', '/footer-(?P<id>[\w-]+)', array(
        'methods' => 'GET',
        'callback' => 'wp_menu_route',
    ) );

    register_rest_route( 'footer', '/all', array(
        'methods' => 'GET',
        'callback' => 'wp_menu_all',
    ) );
} );

remove_filter ('acf_the_content', 'wpautop');

?>