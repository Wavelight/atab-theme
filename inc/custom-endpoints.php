<?php 

function wave_get_pages($data) {
    $posts = get_posts(array(
        'numberposts' => -1,
        'post_type' => array('post', 'project', 'team', 'vacature', 'werkwijze')
    ));
    $pages = get_pages();
    $all = array_merge($pages, $posts);

    /** Fix problem where some hits don't have any slugs */
    foreach($all as $post) {
      if(!$post->template_name) {
        $post->template_name = get_page_template_slug($post);
      }
      if(!$post->slug && $post->post_name) {
          $post->slug = $post->post_name;
      }
      if(!$post->post_name && $post->slug) {
          $post->post_name = $post->slug;
      }
    }
    $response = $all;
    
	return $response;
}

function wave_get_projects() {
  $posts = get_posts(array(
    'post_type' => array('project')
  ));

  foreach($posts as $post) {
    if(!$post->slug && $post->post_name) {
        $post->slug = $post->post_name;
    }
    if(!$post->post_name && $post->slug) {
        $post->post_name = $post->slug;
    }
  }
  return $posts;
}

function get_seo_meta_field( $object, $field_name, $request ) {
  return get_post_meta( $object[ 'id' ], $field_name, true );
}
function create_api_posts_meta_field() {
    
    register_rest_route( 'custom/v1', 'pages/(?P<id>[\w-]+)', array(
      'methods' => 'GET',
      'callback' => 'wave_get_pages'
    ) );
    register_rest_route( 'custom/v1', 'pages/', array(
      'methods' => 'GET',
      'callback' => 'wave_get_pages'
    ) );

    register_rest_route( 'custom/v1', '/frontpage', array(
          'methods'  => 'GET',
          'callback' => 'get_frontpage'
      )
    );

    register_rest_field( ['post', 'project', 'team', 'vacancy', 'page'],
        '_yoast_wpseo_title',
        array(
            'get_callback'    => 'get_seo_meta_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( ['post', 'project', 'team', 'vacancy', 'page'],
        '_yoast_wpseo_metadesc',
        array(
            'get_callback'    => 'get_seo_meta_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( ['post', 'project', 'team', 'vacancy', 'page'],
        '_yoast_wpseo_robots',
        array(
            'get_callback'    => 'get_seo_meta_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
add_action( 'rest_api_init', 'create_api_posts_meta_field' );

// Register Front Page route.
// URL will be: domainname.ext/wp-json/my-namespace/v1/frontpage/

// Callback function.
function get_frontpage( $object ) {

  // Get WP options front page from settings > reading.
  $frontpage_id = get_option('page_on_front');

  // Handle if error.
  if ( empty( $frontpage_id ) ) {
    // return error
    return 'error, no ID';
  }

  // Create request from pages endpoint by frontpage id.
  $request  = new \WP_REST_Request( 'GET', '/wp/v2/pages/' . $frontpage_id );

  // Parse request to get data.
  $response = rest_do_request( $request );

  // Handle if error.
  if ( $response->is_error() ) {
     return 'error';
  }

  return $response->get_data();
}

?>