<?php
    // Enqueue inportant files to wordpress
    function WpFiles_enqueue() {

        // Add Bootstrap css from CDN and our own CSS script
        // wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        // wp_enqueue_style( 'rubik-font', 'https://fonts.googleapis.com/css?family=Rubik:300,400,400i,500, 600,700');

        // wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
        
        // fontawesome icons
        
        //Kit works with pseudo elements, the normal js file did some weird things with icons.
        // wp_enqueue_script( 'fontawesome-defer', 'https://kit.fontawesome.com/bddc133632.js', '', '', true );
        // wp_enqueue_script( 'fa', 'https://pro.fontawesome.com/releases/v5.10.1/css/all.css');

        // Add Bootstrap scripts from CDN
        // wp_enqueue_script( 'bootstrapcdn-async', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );
        // wp_enqueue_script( 'vue', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js', '', '', true );
        //wp_enqueue_script( 'polyfill', get_template_directory_uri() . '/js/vue/polyfill.js', '', '', true );
        // <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.10.1/css/all.css">

        // Our own custom js scripts
        // wp_register_script( 'appjs', get_template_directory_uri() . '/js/app.min.js', array('jquery', 'wp-api'), '', true);

        // wp_localize_script('appjs', 'ipAjaxVar', array(
        //     'ajaxurl' => admin_url('admin-ajax.php'),
        //     'site_url' => get_site_url(),
        //     'bdi' => bdi()
        // ));

        // wp_enqueue_script( 'appjs' );
        // wp_enqueue_script( 'mainjs-defer', get_template_directory_uri() . '/js/vue/main.bundle.js', '', '', true );

        // Add comment reply script if blogpage
        // if ( is_singular() ) wp_enqueue_script( 'comment-reply' );


        // $baseApiPath = get_template_directory_uri() . '/inc/api-data.php';

        // if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        //     $protocol = 'https://';
        // } else {
        //     $protocol = 'http://';
        // }
        // $basePath = $protocol . $_SERVER['HTTP_HOST'];

        // wp_register_script( 'standings', get_template_directory_uri() . '/js/vue/standings.bundle.js', array('wp-api'), '', true );
        // wp_register_script( 'competitionDetail', get_template_directory_uri() . '/js/vue/competitionDetail.bundle.js', array('wp-api'), '', true );
        // wp_register_script( 'clubDetail', get_template_directory_uri() . '/js/vue/clubDetail.bundle.js', array('wp-api'), '', true );
        // wp_register_script( 'tournament', get_template_directory_uri() . '/js/vue/tournament.bundle.js', array('wp-api'), '', true );
        // wp_register_script( 'matchPage', get_template_directory_uri() . '/js/vue/match.bundle.js', array('wp-api'), '', true );
        
        // $additionals = array(
        //     'templateDirectory' => get_template_directory_uri(),
        //     'hostPath' => $basePath,
        //     'baseApiPath' => $baseApiPath,
        //     'wp-api' => 'wp-api'
        // );
        
        // wp_localize_script( 'standings', 'globals', $additionals );
        // wp_localize_script( 'competitionDetail', 'globals', $additionals );
        // wp_localize_script( 'clubDetail', 'globals', $additionals );
        // wp_localize_script( 'tournament', 'globals', $additionals );
        // wp_localize_script( 'matchPage', 'globals', $additionals );

        // global $wp_query;
        // if(is_page()){ //Check if we are viewing a page
        //     //Check which template is assigned to current page we are looking at
        //     $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
        //     switch($template_name) {
        //         case 'homepage.php':
        //             wp_enqueue_script( 'standings' );
        //             break;
                
        //         case 'single-competities.php':
        //             wp_enqueue_script( 'competitionDetail' );

        //             //enqueue file here
        //             break;

        //         case 'single-teams.php':
        //             wp_enqueue_script( 'clubDetail' );
        //             break;

        //         case 'single-toernooicontent.php':
        //             wp_enqueue_script( 'tournament' );
        //             break;
                
        //         case 'single-match.php':
        //             wp_enqueue_script( 'matchPage' );
        //             break;

        //         default:
        //             break;
        //     }
        // } elseif(is_single()) {
            
        //     global $post;
        //     if(get_post_type($post) === 'competities') {
        //         wp_enqueue_script( 'competitionDetail' );
        //     } else if(get_post_type($post) === 'teams') {
        //         wp_enqueue_script( 'clubDetail' );
        //     } else if(get_post_type($post) === 'toernooicontent') {
        //         wp_enqueue_script( 'tournament' );
        //     } else if(get_post_type($post) === 'wedstrijden') {
        //         wp_enqueue_script( 'matchPage' );
        //     }
        // } else {
        //     global $template;
        //     $filename = basename($template);
        //     switch($filename) {
        //         case 'single-match.php':
        //             wp_enqueue_script( 'matchPage' );
        //             break;

        //         default:
        //             break;
        //     }
        // }


        //Disable gutenberg style in Front
        wp_dequeue_style( 'wp-block-library' );

        // Core jQuery in footer
        // wp_scripts()->add_data( 'jquery', 'group', 1 );
        // wp_scripts()->add_data( 'jquery-core', 'group', 1 );
        // wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );

        
    }
    add_action('wp_enqueue_scripts', 'WpFiles_enqueue');

?>