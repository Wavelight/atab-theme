<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'site-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));	
}

//Make textarea go to the bottom in comments
function wpb_move_comment_field_to_bottom( $fields ) {
	//Rearrange order of inputs
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	//unset cookies
	unset( $fields['cookies']);
	return $fields;
}

function my_deregister_styles() {
	wp_deregister_style( 'acf' );
	wp_deregister_style( 'acf-field-group' );
	wp_deregister_style( 'acf-global' );
	wp_deregister_style( 'acf-input' );
	wp_deregister_style( 'acf-datepicker' );
  }
	 
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

// disable acf css on front-end acf forms
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );


do_action('save_post', 'webhook_trigger', 10, 3);

function webhook_trigger () {
	$webhook_url = get_field('webook_url', 'options');
	if($webhook_url) {
		wp_remote_get($webhook_url);
	}
}