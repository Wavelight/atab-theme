<?php
/*
 * Callback function to filter the MCE settings
 */

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

function my_mce_before_init_insert_formats($init_array) {
// Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Kolommen',
            'block' => 'div',
            'classes' => 'columns'
        ),
        array(
            'title' => 'rode tekst',
            'inline' => 'span',
            'classes' => 'red'
        ),
        array(
            'title' => 'groene tekst',
            'inline' => 'span',
            'classes' => 'green'
        ),
        array(
            'title' => 'oranje tekst',
            'inline' => 'span',
            'classes' => 'orange'
        ),
        array(
            'title' => 'blauwe tekst',
            'inline' => 'span',
            'classes' => 'blue'
        ),
        array(
            'title' => 'zwarte tekst',
            'inline' => 'span',
            'classes' => 'black'
        ),
        array(
            'title' => 'link met pijl',
            'inline' => 'span',
            'classes' => 'link-arrow'
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}

// Attach callback to 'tiny_mce_before_init' 
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');
?>