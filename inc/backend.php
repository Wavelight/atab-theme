<?php
require_once get_template_directory() . '/locale.php';

// custom css for login
function my_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/custom-login.css" />';
}
add_action('login_head', 'my_custom_login');
    
// change url of login logo
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
        
function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
    
// remember me to cheched
function login_checked_remember_me() {
    add_filter( 'login_footer', 'rememberme_checked' );
}
add_action( 'init', 'login_checked_remember_me' );
        
function rememberme_checked() {
    echo "<script>document.getElementById('rememberme').checked = true;</script>";
}

// Remove Wordpress version
function wpstarter_remove_wp_version() {
    return '';
    }
add_filter('the_generator', 'wpstarter_remove_wp_version');

// Change the footer in admin panel
function wpstarter_change_admin_footer () {

    echo 'Created with love :)';
    }
add_filter('admin_footer_text', 'wpstarter_change_admin_footer');

// Add automatic copyright
function wpb_copyright() {
    global $wpdb;
    $copyright_dates = $wpdb->get_results("
    SELECT
    YEAR(min(post_date_gmt)) AS firstdate,
    YEAR(max(post_date_gmt)) AS lastdate
    FROM
    $wpdb->posts
    WHERE
    post_status = 'publish' AND post_type = 'post'
    ");
    $output = '';
    if($copyright_dates) {
    $copyright = "© 2010 ";
    if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
    $copyright .= '-' . $copyright_dates[0]->lastdate;
    }
    $output = $copyright;
    }
    return $output;
}

// remove welcome panel from WordPress
remove_action('welcome_panel', 'wp_welcome_panel');

// upload additional file types
function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg'; //Adding svg extension
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

// Disable the emoji's
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
* 
* @param array $plugins 
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
    return array();
    }
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
    /** This filter is documented in wp-includes/formatting.php */
    $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

    $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
}

//Remove JQuery migrate
function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
    $script = $scripts->registered['jquery'];

    if ( $script->deps ) { // Check whether the script has any dependencies
    $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
    }
    }
    }
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

// Remove wp-embed
function my_deregister_scripts(){
    wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

// disable Gutenburg for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable Gutenburg for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/**
* Add async or defer attributes to script enqueues
* @author Mike Kormendy
* @param  String  $tag     The original enqueued <script src="...> tag
* @param  String  $handle  The registered unique name of the script
* @return String  $tag     The modified <script async|defer src="...> tag
*/
// only on the front-end
if(!is_admin()) {
    function add_asyncdefer_attribute($tag, $handle) {
        // if the unique handle/name of the registered script has 'async' in it
        if (strpos($handle, 'async') !== false) {
            // return the tag with the async attribute
            return str_replace( '<script ', '<script async ', $tag );
        }
        // if the unique handle/name of the registered script has 'defer' in it
        else if (strpos($handle, 'defer') !== false) {
            // return the tag with the defer attribute
            return str_replace( '<script ', '<script defer ', $tag );
        }
        // otherwise skip
        else {
            return $tag;
        }
    }
    add_filter('script_loader_tag', 'add_asyncdefer_attribute', 10, 2);
}

// Add a Wordpress knwoledge base to the backend
function knowledge_base_widgets() {

	echo '<script async data-cfasync="false" src="https://d29l98y0pmei9d.cloudfront.net/js/widget.min.js?k=Y2xpZW50SWQ9MTUwNyZob3N0TmFtZT1mdWdhbWVkaWEuc3VwcG9ydGhlcm8uaW8="></script>';

}

add_action('admin_footer', 'knowledge_base_widgets');


add_action( 'admin_menu', 'api_control_panel' );

function api_control_panel() {
	add_options_page( 'Deploy control panel', 'Deploy control panel', 'manage_options', 'api-control-panel', 'api_control_panel_contents', 1 );
    // acf_add_options_page(array('page_title' 	=> 'API Control Panel','menu_title'	=> 'API Control Panel','menu_slug' 	=> 'api-control-panel','capability'	=> 'manage_options','redirect'		=> false,'position'      => 'site-options'));
    // add_submenu_page( 'page=site-options', 'API Control Panel', 'API Control Panel', 'manage_options', 'api-control-panel', 'api_control_panel_contents' );
}

//prompt function
function prompt($prompt_msg){
    echo("<script type='text/javascript'> var answer = confirm('".$prompt_msg."'); </script>");

    $answer = "<script type='text/javascript'> document.write(answer); </script>";
    return($answer);
}

function api_control_panel_contents() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    echo '<div class="wrap" style="margin-top: 100px;">
        <form method="post" action="' . $actual_link . '">
            <div>    
                Deploy website
            </div>
            <input type="submit" value="Deploy website" name="deploy_website" onClick="javascript:return confirm(\'are you sure you want to deploy the website?\');" style="background-color:#1153c6;color:#FFFFFF;padding:10px 20px;border-radius:10px;text-transform:uppercase;font-weight:800; cursor:pointer;" onMouseOver="this.style.backgroundColor=\'green\'" onMouseOut="this.style.backgroundColor=\'#1153c6\'">
        </form>
    </div>';
}
if(isset($_POST['deploy_website'])) {
    // curl -X POST -d {} https://api.netlify.com/build_hooks/5ea43773a4028da3a338c3f2


    $url = 'https://api.netlify.com/build_hooks/5ea43773a4028da3a338c3f2';

    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }

}

function wpb_set_meta_key($postID, $_metaKey, $_value) {
	$meta_key = $_metaKey;
	$currentValue = get_post_meta($postID, $meta_key, true);

	if($currentValue==''){
		delete_post_meta($postID, $meta_key);
		add_post_meta($postID, $meta_key, $_value);
	}else{
		update_post_meta($postID, $meta_key, $_value);
	}
}

//ACF load
if(strpos($_SERVER['SERVER_NAME'], 'voetbaluitslagen.com') == 1) {
    add_filter('acf/settings/load_json', 'my_acf_json_load_point');
}
function my_acf_json_load_point( $paths ) {
    // remove original path (optional)
    unset($paths[0]);
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    // return
    return $paths;
}
?>