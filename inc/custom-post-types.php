<?php

function vacature_post_type() {
	$labels = array(
		'name' 					=> 'Vacature',
		'singular_name' 		=> 'Vacature'
	);

	$args = array(
		'label'                 => 'Vacatures',
		'description'           => 'Een overzicht van alle vacatures',
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-media-document',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'show_in_rest'			=> true,		
		'rewrite'               => array(
			'slug'	=> 'vacatures',
			'with_front'  => false,
		),
		'capability_type'       => 'page',
	);

	register_post_type( 'vacature', $args );

}

add_action( 'init', 'vacature_post_type', 0 );

function project_post_type() {
	$labels = array(
		'name' 					=> 'Projecten',
		'singular_name' 		=> 'Project'
	);

	$args = array(
		'label'                 => 'Projecten',
		'description'           => 'Een overzicht van alle projecten',
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-tools',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'show_in_rest'			=> true,		
		'rewrite'               => array(
			'slug'	=> 'projecten',
			'with_front'  => false,
		),
		'capability_type'       => 'page',
	);

	register_post_type( 'project', $args );

}

add_action( 'init', 'project_post_type', 0 );

function team_post_type() {
	$labels = array(
		'name' 					=> 'Team',
		'singular_name' 		=> 'Team'
	);

	$args = array(
		'label'                 => 'Team',
		'description'           => 'Een overzicht van alle werknemers',
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'show_in_rest'			=> true,		
		'rewrite'               => array(
			'slug'	=> 'team',
			'with_front'  => false,
		),
		'capability_type'       => 'page',
	);

	register_post_type( 'team', $args );

}

add_action( 'init', 'team_post_type', 0 );


// remove slugs for CPT
function na_remove_slug_example( $post_link, $post, $leavename ) {

    if ( 'team' != $post->post_type || 'project' != $post->post_type || 'vacature' != $post->post_type ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'na_remove_slug_example', 10, 3 );

// Not getting a 404 error while removing slug
function na_parse_request( $query ) {

		// Bail if this is not the main query.
		if ( ! $query->is_main_query() ) {
			return;
		}
		// Bail if this query doesn't match our very specific rewrite rule.
		if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
			return;
		}
		// Bail if we're not querying based on the post name.
		if ( empty( $query->query['name'] ) ) {
			return;
		}
	
	
		// Add CPT to the list of post types WP will include when it queries based on the post name.
		$query->set( 'post_type', array( 'post', 'page', 'project', 'team', 'vacature' ) );
}
add_action( 'pre_get_posts', 'na_parse_request' );
if(!is_admin()) {
	add_action( 'pre_get_posts', 'na_parse_request' );
}

/**
 * This function modifies the main WordPress query to include an array of 
 * post types instead of the default 'post' post type.
 *
 * @param object $query  The original query.
 * @return object $query The amended query.
 */
function tgm_io_cpt_search( $query ) {
	
	if ( $query->is_search ) {
		$query->set( 'post_type', array( 'post', 'page', 'team', 'project','vacature' ) );
    }
    
    return $query;
    
}
if(!is_admin()) {	
	add_filter( 'pre_get_posts', 'tgm_io_cpt_search' );
}

add_filter( 'rest_project_query', 'se35728943_change_post_per_page', 10, 2 );
add_filter( 'rest_team_query', 'se35728943_change_post_per_page', 10, 2 );
add_filter( 'rest_vacature_query', 'se35728943_change_post_per_page', 10, 2 );

function se35728943_change_post_per_page( $args, $request ) {
    $max = max( (int) $request->get_param( 'custom_per_page' ), 2000 );
    $args['posts_per_page'] = $max;    
    return $args;
}

add_filter( 'acf_archive_post_types', 'change_acf_archive_cpt' );
function change_acf_archive_cpt( $cpts ) {
	// 'book' and 'movie' are the cpt key.

	// Remove cpt
	unset( $cpts['Example'] );

	// Add cpt
	$cpts['Example'] = 'Example Archive';

	return $cpts;
}

?>