<?php get_header();  ?>
  
<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <div class="container">
      <div class="row">
      
      <?php get_template_part( 'template-parts/content', 'search' ); ?> 

      <?php get_template_part( 'template-parts/pagination' ); ?> 
      
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>