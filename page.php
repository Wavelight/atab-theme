<?php get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <div class="container" style="padding: 2% 15px;">

     <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>

      <?php endwhile;  endif;?>

      <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
          comments_template();
        endif;
      ?>
     
    </div>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>