<?php get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main site-main--404 lazyload" data-bgset="<?= bdi('/stadium.jpg'); ?>">

    <div class="container">
      <div class="row">
        <div class="centered">
          <img data-src="<?= bdi('/404/404.png'); ?>" alt="404 voetbaluitslagen" class="lazyload img-fluid centered__image">
          <p class="centered__text">Er is een fout opgetreden tijdens het verwerken van uw verzoek.</p>
          <?php
            $link = get_site_url();
          ?>

          <div class="button--wrapper">
              <a class="button" href="<?= $link ?>">
                <div class="button__text">
                  terug naar de homepage
                </div>
                <div class="button__icon">
                  <i class="fas fa-chevron-right"></i>
                </div>
              </a>
          </div>
        </div>
        
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>