<?php

    require_once get_template_directory() . '/inc/enqueue.php';
    require_once get_template_directory() . '/inc/navwalker.php';
    require_once get_template_directory() . '/inc/core-functions.php';
    require_once get_template_directory() . '/inc/backend.php';
    require_once get_template_directory() . '/inc/pagination-function.php';
    require_once get_template_directory() . '/inc/custom-comments.php';
    require_once get_template_directory() . '/inc/custom-post-types.php';
    require_once get_template_directory() . '/inc/custom-taxonomy.php';
    require_once get_template_directory() . '/inc/tgm-plugins.php';
    require_once get_template_directory() . '/inc/theme-options.php';
    require_once get_template_directory() . '/inc/popular-posts.php';
    require_once get_template_directory() . '/inc/sort-columns.php';
    require_once get_template_directory() . '/inc/custom-endpoints.php';
    // require_once get_template_directory() . '/inc/activate-plugin.php';
    require_once get_template_directory() . '/inc/wysiwyg-classes.php';   
    
    function add_cors_http_header(){
        header("Access-Control-Allow-Origin: *");
    }
    add_action('init','add_cors_http_header');
    add_action( 'rest_api_init', 'add_cors_http_header' );
?>