<?php 

/**
 * Template Name: Content pagina
 *
 * @package WordPress
 * @subpackage WP-Starter-Theme
 * @since v1.0
 */


get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main site-main--offline lazyload">

  <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="centered">
            <h1 class="centered__heading intro__title">Geen verbinding</h1>
            <p class="centered__text">Het lijkt er op dat je momenteel geen verbinding hebt.  Wacht tot je weer online bent om deze pagina te bekijken.</p>
            <?php
              $link = get_site_url();
            ?>

            <div class="button--wrapper">
                <a class="button" href="<?= $link ?>">
                  <div class="button__text">
                    terug naar de homepage
                  </div>
                  <div class="button__icon">
                    <i class="fas fa-chevron-right"></i>
                  </div>
                </a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="centered centered__image--container">
            <!-- Place image here if you want -->
          </div>
        </div>
        
      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>