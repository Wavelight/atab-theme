
<footer id="footer" class="footer js-footer">

    <div class="container">
        <div class="row">

            <div class="col-12 col-md-6 col-lg-3 footer__widget">
                <?php dynamic_sidebar( 'footer-1' ); ?>
            </div>

            <div class="col-12 col-md-6 col-lg-3 footer__widget">
                <?php
                    if(dynamic_sidebar( 'footer-2' )):
                        dynamic_sidebar( 'footer-2' );
                    else:
                        $query = [
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => 8,
                            'orderby' => 'publish_date',
                            'order' => 'DESC'
                        ];

                        $loop = new WP_Query($query);

                        echo '<div class="actueel">';
                        echo '<h5 class="actueel__title">Actueel</h5>';
                        echo '<ul class="actueel__list">';

                            foreach($loop->posts as $item):
                            ?>
                                <li class="actueel__item">
                                    <a href="<?= get_the_permalink($item->ID); ?>" class="actueel__link">
                                        <?php
                                            $c = strlen($item->post_title);

                                            if($c > 30):
                                                echo substr($item->post_title, 0, 30).'...';
                                            else:
                                                echo $item->post_title;
                                            endif;
                                            
                                        ?>
                                    </a>
                                </li>
                            <?php
                            endforeach;
                        echo '</ul>';
                        echo '</div>';
                    endif;
                ?>
            </div>

            <?php
                if(dynamic_sidebar( 'footer-3' )):
            ?>

            <div class="col-12 col-md-4 col-lg-3 footer__widget">
                <?php dynamic_sidebar( 'footer-3' ); ?>
            </div>

            <div class="col-12 col-md-12 col-lg-3 footer__widget">
                <?php dynamic_sidebar( 'footer-4' ); ?>
            </div>

            <?php
                else:
            ?>
            <div class="col-12 col-md-12 col-lg-6 footer__widget">
                <!-- <div class="banner">
                    <a href="#" class="banner__link">
                        <img data-src="<?= bdi('/footer/footer-banner-voorbeeld.jpg'); ?>" alt="" class="lazyload img-fluid banner__image">
                    </a>
                </div> -->
            </div>
            <?php
                endif;
            ?>

        </div>
    </div>

    <div class="footer__brand">
        <img class="lazyload" data-src="<?= bdi('/voetbaluitslagen-logo.svg');?>" height="30px" alt="<?php echo get_bloginfo( 'name' ); ?>"/>
    </div>

    <div class="footer__copyright">
        <?php echo wpb_copyright(); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo( 'name' ); ?></a>
    </div>

    <?php
        $footer_socials = get_field('footer_socials', 'options');

        if($footer_socials):
            set_query_var('footer_socials', $footer_socials);
            get_template_part('template-parts/content/social-icons');
        endif;
    ?>

</footer>

<!-- Cookie warning -->
<?php require "template-parts/cookies.php"; ?>

<?php wp_footer(); ?>

</body>
</html>