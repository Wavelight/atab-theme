<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <title><?php echo get_bloginfo( 'name' ); ?> - <?php echo get_bloginfo( 'description' ); ?></title>

  <!-- Favicon -->
  <?php if ( get_field( 'header-favicon', 'option' ) ) { ?>
    <!-- Use the custom logo from the backend -->
    <link rel="shortcut icon" href="<?php the_field( 'header-favicon', 'option' ); ?>" >
  <?php } else { ?>
    <!-- Use the default logo from theme folder -->
    <link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/favicon.ico" > 
  <?php } ?>

  <?php wp_head(); ?>

  <?php if ( get_field( 'google-tagmanager-id', 'option' ) ) { ?>
  <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php the_field( 'google-tagmanager-id', 'option' ); ?>');</script>
  <!-- End Google Tag Manager -->
  <?php } ?>

  <?php if ( get_field( 'hotjar-id', 'option' ) ) { ?>
  <!-- Hotjar Tracking Code for https://www.voetbaluitslagen.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:<?php the_field( 'hotjar-id', 'option' ); ?>,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
  <?php } ?>
  <script>
      window.FontAwesomeConfig = {
        searchPseudoElements: true,
        keepOriginalSource: false
      }
    </script>
  <script src="https://kit.fontawesome.com/bddc133632.js"></script>
</head>
<body>

<?php if ( get_field( 'google-tagmanager-id', 'option' ) ) { ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php the_field( 'google-tagmanager-id', 'option' ); ?>"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
<?php } ?>

<?php if ( get_field( 'piwik-tracking-url', 'option') && get_field( 'piwik-tracking-id', 'option')) { ?>
  <script type="text/javascript">
  (function(window, document, dataLayerName, id) {
  window[dataLayerName]=window[dataLayerName]||[],window[dataLayerName].push({start:(new Date).getTime(),event:"stg.start"});var scripts=document.getElementsByTagName('script')[0],tags=document.createElement('script');
  function stgCreateCookie(a,b,c){var d="";if(c){var e=new Date;e.setTime(e.getTime()+24*c*60*60*1e3),d="; expires="+e.toUTCString()}document.cookie=a+"="+b+d+"; path=/"}
  var isStgDebug=(window.location.href.match("stg_debug")||document.cookie.match("stg_debug"))&&!window.location.href.match("stg_disable_debug");stgCreateCookie("stg_debug",isStgDebug?1:"",isStgDebug?14:-1);
  var qP=[];dataLayerName!=="dataLayer"&&qP.push("data_layer_name="+dataLayerName),isStgDebug&&qP.push("stg_debug");var qPString=qP.length>0?("?"+qP.join("&")):"";
  tags.async=!0,tags.src="<?php the_field( 'piwik-tracking-url', 'option' );?>/"+id+".js"+qPString,scripts.parentNode.insertBefore(tags,scripts);
  !function(a,n,i){a[n]=a[n]||{};for(var c=0;c<i.length;c++)!function(i){a[n][i]=a[n][i]||{},a[n][i].api=a[n][i].api||function(){var a=[].slice.call(arguments,0);"string"==typeof a[0]&&window[dataLayerName].push({event:n+"."+i+":"+a[0],parameters:[].slice.call(arguments,1)})}}(i[c])}(window,"ppms",["tm","cm"]);
  })(window, document, 'dataLayer', '<?php the_field( 'piwik-tracking-id', 'option' );?>');
  </script><noscript><iframe src="<?php the_field( 'piwik-tracking-url', 'option' );?>/<?php the_field( 'piwik-tracking-id', 'option' );?>/noscript.html" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php } ?>

<header>

  </header>